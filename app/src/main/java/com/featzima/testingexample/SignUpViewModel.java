package com.featzima.testingexample;


import android.text.TextUtils;
import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

public class SignUpViewModel {

    Subject<String> firstName;
    Subject<String> lastName;
    BehaviorSubject<String> fullName;
    Subject<Boolean> isValidated;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SignUpViewModel() {
        firstName = BehaviorSubject.createDefault("");
        lastName = BehaviorSubject.createDefault("");
        fullName = BehaviorSubject.create();
        isValidated = BehaviorSubject.create();

        compositeDisposable.add(Observable.combineLatest(
                firstName,
                lastName,
                new BiFunction<String, String, Boolean>() {
                    @Override
                    public Boolean apply(String s, String s2) throws Exception {
                        return !TextUtils.isEmpty(s) && !TextUtils.isEmpty(s2);
                    }
                })
                .observeOn(Schedulers.io())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        isValidated.onNext(aBoolean);
                    }
                }));
        compositeDisposable.add(Observable.combineLatest(
                firstName,
                lastName,
                new BiFunction<String, String, String>() {
                    @Override
                    public String apply(String s, String s2) throws Exception {
                        return s + " " + s2;
                    }
                })
                .observeOn(Schedulers.io())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String result) throws Exception {
                        fullName.onNext(result);
                    }
                }));
    }

    public void dispose() {
        compositeDisposable.dispose();
    }
}
