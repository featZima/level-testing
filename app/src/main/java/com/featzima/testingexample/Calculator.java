package com.featzima.testingexample;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    private List<Integer> currentValues;

    public Calculator() {
        currentValues = new ArrayList<>();
        currentValues.add(0);
    }

    public void addValue(int value) {
        currentValues.add(value);
    }

    public void operationAdd() {
        int a = currentValues.get(currentValues.size() - 2);
        int b = currentValues.get(currentValues.size() - 1);
        int c = a + b;
        currentValues.clear();
        currentValues.add(c);
    }

    public int getCurrentValue() {
        return currentValues.get(currentValues.size() - 1);
    }
}
