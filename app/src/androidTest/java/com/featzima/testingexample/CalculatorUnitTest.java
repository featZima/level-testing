package com.featzima.testingexample;

import androidx.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class CalculatorUnitTest {

    @Test
    public void testCurrentValue() {
        // init
        Calculator calculator = new Calculator();

        // action
        calculator.addValue(2);

        // assert
        assertEquals(2, calculator.getCurrentValue());
    }

    @Test
    public void testOperationAdd() {
        // init
        Calculator calculator = new Calculator();
        calculator.addValue(2);
        calculator.addValue(2);

        // action
        calculator.operationAdd();

        // assert
        assertEquals(4, calculator.getCurrentValue());
    }

    @Test
    public void testDefaultValue() {
        // init
        Calculator calculator = new Calculator();

        // action

        // assert
        assertEquals(0, calculator.getCurrentValue());
    }

    @Test
    public void testUnaryAddition() {
        // init
        Calculator calculator = new Calculator();

        // action
        calculator.addValue(2);
        calculator.operationAdd();

        // assert
        assertEquals(2, calculator.getCurrentValue());
    }

}
