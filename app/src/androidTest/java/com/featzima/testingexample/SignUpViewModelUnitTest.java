package com.featzima.testingexample;

import androidx.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import io.reactivex.observers.TestObserver;

@RunWith(AndroidJUnit4.class)
public class SignUpViewModelUnitTest {

    @Test
    public void testFullName() {
        // init
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        TestObserver<String> testSubscriber = new TestObserver<>();
        signUpViewModel.fullName.subscribe(testSubscriber);

        // action
        signUpViewModel.firstName.onNext("Dmytro");
        signUpViewModel.lastName.onNext("Glynskyi");

        // assert
        testSubscriber.awaitCount(3);
        testSubscriber.assertValueAt(2, "Dmytro Glynskyi");

        signUpViewModel.dispose();
    }

    @Test
    public void testInitialFullName() {
        // init
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        TestObserver<String> testSubscriber = new TestObserver<>();
        signUpViewModel.fullName.subscribe(testSubscriber);

        // action

        // assert
        testSubscriber.awaitCount(1);
        testSubscriber.assertValue(" ");

        signUpViewModel.dispose();
    }

    @Test
    public void testInvalidStateForInitialState() {
        // init
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        TestObserver<Boolean> testSubscriber = new TestObserver<>();
        signUpViewModel.isValidated.subscribe(testSubscriber);

        // action

        // assert
        testSubscriber.awaitCount(1);
        testSubscriber.assertValueAt(0, false);

        signUpViewModel.dispose();
    }

    @Test
    public void testInvalidStateForEmptyFirstName() {
        // init
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        TestObserver<Boolean> testSubscriber = new TestObserver<>();
        signUpViewModel.isValidated.subscribe(testSubscriber);

        // action
        signUpViewModel.firstName.onNext("some name");
        signUpViewModel.firstName.onNext("");

        // assert
        testSubscriber.awaitCount(3);
        testSubscriber.assertValueAt(2, false);

        signUpViewModel.dispose();
    }


    @Test
    public void testInvalidStateForEmptyLastName() {
        // init
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        TestObserver<Boolean> testSubscriber = new TestObserver<>();
        signUpViewModel.isValidated.subscribe(testSubscriber);

        // action
        signUpViewModel.lastName.onNext("some name");
        signUpViewModel.lastName.onNext("");

        // assert
        testSubscriber.awaitCount(3);
        testSubscriber.assertValueAt(2, false);

        signUpViewModel.dispose();
    }

    @Test
    public void testValidState() {
        // init
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        TestObserver<Boolean> testSubscriber = new TestObserver<>();
        signUpViewModel.isValidated.subscribe(testSubscriber);

        // action
        signUpViewModel.firstName.onNext("firstName");
        signUpViewModel.lastName.onNext("lastName");

        // assert
        testSubscriber.awaitCount(3);
        testSubscriber.assertValueAt(2, true);

        signUpViewModel.dispose();
    }

}
